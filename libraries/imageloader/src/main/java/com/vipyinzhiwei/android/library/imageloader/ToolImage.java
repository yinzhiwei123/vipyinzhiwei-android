package com.vipyinzhiwei.android.library.imageloader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

/**
 * 图片加载工具类
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class ToolImage {
    /**
     * 日志输出标识
     */
    protected final static String TAG = ToolImage.class.getClass().getSimpleName();

    private static ToolImage instance;

    public static ToolImage getInstance() {
        if (instance == null) {
            synchronized (ToolImage.class) {
                if (instance == null) {
                    instance = new ToolImage();
                }
            }
        }
        return instance;
    }

    /**
     * 初始化Glide配置
     *
     * @param mContext 上下文，建议用Application
     * @return
     */
    public static void init(Context mContext) {


    }

    /**
     * 加载本地图片
     *
     * @param context
     * @param file
     * @param iv
     */
    public static void load(Context context, File file, ImageView iv) {
        Glide.with(context).load(file).into(iv);
    }

    /**
     * 加载本地图片
     *
     * @param context
     * @param file
     * @param options
     * @param iv
     */
    public static void load(Context context, File file, RequestOptions options, ImageView iv) {
        Glide.with(context).load(file).apply(options).into(iv);
    }

    /**
     * 加载应用资源
     *
     * @param context
     * @param resId
     * @param iv
     */
    public static void load(Context context, int resId, ImageView iv) {
        Glide.with(context).load(resId).into(iv);
    }

    /**
     * 加载应用资源
     *
     * @param context
     * @param resId
     * @param options
     * @param iv
     */
    public static void load(Context context, int resId, RequestOptions options, ImageView iv) {
        Glide.with(context).load(resId).apply(options).into(iv);
    }

    /**
     * 加载二进制流
     *
     * @param context
     * @param image
     * @param iv
     */
    public static void load(Context context, byte[] image, ImageView iv) {
        Glide.with(context).load(image).into(iv);
    }

    /**
     * 加载二进制流
     *
     * @param context
     * @param image
     * @param iv
     */
    public static void load(Context context, byte[] image, RequestOptions options, ImageView iv) {
        Glide.with(context).load(image).apply(options).into(iv);
    }

    /**
     * 加载网络图片
     *
     * @param context
     * @param url
     * @param iv
     */
    public static void load(Context context, String url, ImageView iv) {
        Glide.with(context).load(url).into(iv);
    }

    /**
     * 加载网络图片
     *
     * @param context
     * @param url
     * @param options
     * @param iv
     */
    public static void load(Context context, String url, RequestOptions options, ImageView iv) {
        Glide.with(context).load(url).apply(options).into(iv);
    }

    /**
     * 获取默认显示配置选项
     */
    public static RequestOptions getDefaultOptions() {
        RequestOptions options = new RequestOptions();
        options.placeholder(R.drawable.anl_common_default_picture);    //占位图
        options.error(R.drawable.anl_common_default_picture);          //异常占位图
        return options;
    }

    /**
     * 获取圆角头像配置选项
     */
    public static RequestOptions getRoundOptions() {
        RequestOptions options = new RequestOptions();
        options.circleCrop();
        return options;
    }

    /**
     * 禁止缓存（内存+硬盘）Option
     *
     * @return
     */
    public RequestOptions getDisableCachingOptions() {
        RequestOptions options = new RequestOptions();
        //禁用掉 Glide 的内存缓存功能。默认情况下，Glide自动就是开启内存缓存的
        options.skipMemoryCache(true);
        //禁止Glide对图片进行硬盘缓存。默认情况下，缓存转换过后的图片
        options.diskCacheStrategy(DiskCacheStrategy.NONE);
        return options;
    }

    /**
     * 清除内存缓存,请在主线程中调用
     */
    public static void clearMemoryCache(Context context) {
        Glide.get(context).clearMemory();
    }

    /**
     * 清除硬盘缓存,请在非UI线程中调用
     */
    public static void clearDiskCache(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(context).clearDiskCache();
            }
        }).start();
    }

    /**
     * 清除所有缓存
     */
    public static void clearCache(final Context context) {
        clearMemoryCache(context);
        clearDiskCache(context);
    }
}
