package com.vipyinzhiwei.android.library.imageloader.listener;

import android.content.Context;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.bumptech.glide.Glide;

/**
 * ListView滚动监听，对图片加载进行判断处理
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class PauseListViewOnScrollListener implements AbsListView.OnScrollListener {

    private final Context mContext;
    private final boolean pauseOnScroll;
    private final boolean pauseOnFling;
    private final AbsListView.OnScrollListener externalListener;

    /**
     * Constructor
     *
     * @param context
     * @param pauseOnScroll
     * @param pauseOnFling
     */
    public PauseListViewOnScrollListener(Context context, boolean pauseOnScroll, boolean
            pauseOnFling) {
        this(context, pauseOnScroll, pauseOnFling, null);
    }

    /**
     * Constructor
     *
     * @param context
     * @param pauseOnScroll
     * @param pauseOnFling
     * @param customListener
     */
    public PauseListViewOnScrollListener(Context context, boolean pauseOnScroll, boolean
            pauseOnFling,
                                         AbsListView.OnScrollListener customListener) {
        this.mContext = context;
        this.pauseOnScroll = pauseOnScroll;
        this.pauseOnFling = pauseOnFling;
        this.externalListener = customListener;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (mContext == null) return;

        switch (scrollState) {
            //滚动停止时的状态
            case OnScrollListener.SCROLL_STATE_IDLE:
                Glide.with(mContext).resumeRequests();
                break;

            //惯性滚动状态
            case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                if (pauseOnFling) {
                    Glide.with(mContext).pauseRequests();
                }
                break;

            //正在滚动，用户手指在屏幕上
            case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                if (pauseOnScroll) {
                    Glide.with(mContext).pauseRequests();
                }
                break;
        }
        if (externalListener != null) {
            externalListener.onScrollStateChanged(view, scrollState);
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int
            totalItemCount) {
        if (externalListener != null) {
            externalListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }
}
