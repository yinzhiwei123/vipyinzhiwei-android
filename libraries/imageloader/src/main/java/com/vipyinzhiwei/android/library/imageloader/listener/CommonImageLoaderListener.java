/*
 *     Android基础开发个人积累、沉淀、封装、整理共通
 *     Copyright (c) 2016. 曾繁添 <zftlive@163.com>
 *     Github：https://github.com/zengfantian || http://git.oschina.net/zftlive
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

package com.vipyinzhiwei.android.library.imageloader.listener;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.vipyinzhiwei.android.library.imageloader.ToolImage;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 常用图片加载监听器,加载取消或者失败会将ImageView的ScaleType设置成FIX_XY
 *
 * @author 曾繁添
 * @version 1.0
 */
public class CommonImageLoaderListener implements RequestListener<ImageView> {

    /**
     * 是否第一次加载集合
     */
    protected final static List<String> displayedImages = Collections
            .synchronizedList(new LinkedList<String>());
    /**
     * 是否需要渐现
     */
    private boolean isFade = false;

    /**
     * 日志输出标识
     */
    protected final static String TAG = ToolImage.class.getClass().getSimpleName();

//    private static CommonImageLoaderListener instance;
//
//    private CommonImageLoaderListener() {
//
//    }
//
//    public static CommonImageLoaderListener getInstance() {
//        if (instance == null) {
//            synchronized (CommonImageLoaderListener.class) {
//                if (instance == null) {
//                    instance = new CommonImageLoaderListener();
//                }
//            }
//        }
//        return instance;
//    }

    public CommonImageLoaderListener() { }

    public CommonImageLoaderListener(boolean isFade) {
        this.isFade = isFade;
    }


    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean
            isFirstResource) {
        if (null != target && null != target) {
            ImageView imageView = (ImageView) target;
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        return false;
    }


    @Override
    public boolean onResourceReady(ImageView resource, Object model, Target<ImageView> target,
                                   DataSource dataSource, boolean isFirstResource) {
        if (null != resource && null != model) {
            ImageView imageView = (ImageView) resource;
            // 是否第一次显示
            if (isFade && !displayedImages.contains((String) model)) {
                animate(imageView, 200);
                displayedImages.add((String) model);
            }
        }
        return false;
    }


    /**
     * 清除图片
     */
    public static void clearDisplayedImages() {
        displayedImages.clear();
    }

    /**
     * 清除图片
     */
    public static void clearDisplayedImages(String imageURL) {
        displayedImages.remove(imageURL);
    }

    /**
     * Animates {@link ImageView} with "fade-in" effect
     *
     * @param imageView      {@link ImageView} which display image in
     * @param durationMillis The length of the animation in milliseconds
     */
    public static void animate(View imageView, int durationMillis) {
        if (imageView != null) {
            AlphaAnimation fadeImage = new AlphaAnimation(0, 1);
            fadeImage.setDuration(durationMillis);
            fadeImage.setInterpolator(new DecelerateInterpolator());
            imageView.startAnimation(fadeImage);
        }
    }

}
