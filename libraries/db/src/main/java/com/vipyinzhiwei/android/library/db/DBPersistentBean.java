package com.vipyinzhiwei.android.library.db;

import org.litepal.crud.LitePalSupport;

import java.io.Serializable;

/**
 * 数据持久化实体Bean
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class DBPersistentBean extends LitePalSupport implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2805831017297588164L;

    /**
     * 主键ID
     */
    public int id;

    /**
     * 备注
     */
    public String remark;

    /**
     * 版本号
     */
    public Integer version;

    /**
     * 是否有效
     */
    public Boolean valid = true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
