package com.vipyinzhiwei.android.library.tools.uipage;

import com.vipyinzhiwei.android.library.base.ui.IActivity;
import com.vipyinzhiwei.android.library.base.ui.IFragment;

/**
 * 横向切入的UI（Activity,Fragment）生命周期方法管理帮助类
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class UImanagerHelper {
    private static IActivity sIActivity;
    private static IFragment sIFragment;

    /**
     * 注入Activity生命周期方法管理类，根据业务需求是否实现友盟统计功能。
     *
     * @param sIActivity
     */
    public static void setsIActivity(IActivity sIActivity) {
        UImanagerHelper.sIActivity = sIActivity;
    }

    /**
     * 注入Fragment生命周期方法管理类，根据业务需求是否实现友盟统计功能
     *
     * @param sIFragment
     */
    public static void setsIFragment(IFragment sIFragment) {
        UImanagerHelper.sIFragment = sIFragment;
    }

    /**
     * 获取注入的Activity生命周期方法管理类
     *
     * @return 默认返回未处理的DefaultActivityImpl实现类
     */
    public static IActivity getIActivity() {
        if (sIActivity == null) {
            return new DefaultIActivityImpl();
        }
        return sIActivity;
    }

    /**
     * 获取注入的Fragment生命周期方法管理类
     *
     * @return 默认返回未处理的DefaultFragmentImpl实现类
     */
    public static IFragment getIFragment() {
        if (sIFragment == null) {
            return new DefaultIFragmentImpl();
        }

        return sIFragment;
    }
}
