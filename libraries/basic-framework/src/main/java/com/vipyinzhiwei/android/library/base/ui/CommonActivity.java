package com.vipyinzhiwei.android.library.base.ui;

import android.content.Context;
import android.os.Bundle;

/**
 * 一般常用的Activity
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public abstract class CommonActivity extends BaseActivity{

    @Override
    public void initParams(Bundle params) {

    }

    @Override
    public void doBusiness(Context mContext) {

    }
}
