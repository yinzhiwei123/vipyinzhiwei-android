package com.vipyinzhiwei.android.library.base;

import android.view.View;

/**
 * 蒙层点击监听事件，点击事件处理引导的业务
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public interface IMaskClickListener extends View.OnClickListener{
}
