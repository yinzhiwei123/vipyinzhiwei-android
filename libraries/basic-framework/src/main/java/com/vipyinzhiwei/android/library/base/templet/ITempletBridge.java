package com.vipyinzhiwei.android.library.base.templet;

import android.content.Context;

/**
 * 视图模板与UI交互桥梁
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public interface ITempletBridge {
    /**
     * 获取依赖引用的Context
     * @return
     */
    Context getRefContext();
}
