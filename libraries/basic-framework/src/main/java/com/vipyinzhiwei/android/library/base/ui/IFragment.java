package com.vipyinzhiwei.android.library.base.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 横向切入的Fragment生命周期方法管理类，所有方法与Fragment生命周期方法一一对应。目前仅提供给友盟统计分析使用。后续根据业务需求扩展。
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public interface IFragment {

    void onAttach(Fragment fragment);

    void onCreate(Fragment fragment, Bundle savedInstanceState);

    void onCreateView(Fragment fragment, LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState);

    void onActivityCreated(Fragment fragment, Bundle savedInstanceState);

    void onViewCreated(Fragment fragment, View view, Bundle savedInstanceState);

    void onStart(Fragment fragment);

    /**
     * 提供给友盟统计使用
     *
     * @param fragment
     */
    void onResume(Fragment fragment);

    /**
     * 提供给友盟统计使用
     *
     * @param fragment
     */
    void onPause(Fragment fragment);

    void onStop(Fragment fragment);

    void onDestroy(Fragment fragment);

    void onDetach(Fragment fragment);

    void onDestroyView(Fragment fragment);
}
