package com.vipyinzhiwei.android.library.tools.uipage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.vipyinzhiwei.android.library.base.ui.IActivity;

/**
 * 提供给友盟统计分析使用的Activity生命周期方法管理类，默认实现类。
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class DefaultIActivityImpl implements IActivity {

    @Override
    public void onCreate(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onNewIntent(Activity activity, Intent intent) {

    }

    @Override
    public void onRestart(Activity activity) {

    }

    @Override
    public void onStart(Activity activity) {

    }

    @Override
    public void onResume(Activity activity) {

    }

    @Override
    public void onPause(Activity activity) {

    }

    @Override
    public void onStop(Activity activity) {

    }

    @Override
    public void onDestroy(Activity activity) {

    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {

    }
}
