package com.vipyinzhiwei.android.library.base.templet;

import com.vipyinzhiwei.android.library.base.IBaseConstant;

/**
 * 视图模板基础业务常量
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public interface ITempletConstant extends IBaseConstant.IColor {
    /**
     * 方向-左
     */
    int DIRECTION_LEFT = -1;

    /**
     * 方向-中间
     */
    int DIRECTION_CENTER = 0;

    /**
     * 方向-右
     */
    int DIRECTION_RIGHT = 1;

    /**
     * 指示器默认颜色
     */
    String UN_SELECTED_DOT_COLOR = "#E8E8E8";

    /**
     * 指示器选中颜色
     */
    String SELECTED_DOT_COLOR = COLOR_666666;
}
