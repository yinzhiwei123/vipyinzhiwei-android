package com.vipyinzhiwei.android.library.tools.uipage;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vipyinzhiwei.android.library.base.ui.IFragment;

/**
 * 提供给友盟统计分析使用的Fragment生命周期方法管理类，默认实现类。
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class DefaultIFragmentImpl implements IFragment {

    @Override
    public void onAttach(Fragment fragment) {

    }

    @Override
    public void onCreate(Fragment fragment, Bundle savedInstanceState) {

    }

    @Override
    public void onCreateView(Fragment fragment, LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

    }

    @Override
    public void onActivityCreated(Fragment fragment, Bundle savedInstanceState) {

    }

    @Override
    public void onViewCreated(Fragment fragment, View view, Bundle savedInstanceState) {

    }

    @Override
    public void onStart(Fragment fragment) {

    }

    @Override
    public void onResume(Fragment fragment) {

    }

    @Override
    public void onPause(Fragment fragment) {

    }

    @Override
    public void onStop(Fragment fragment) {

    }

    @Override
    public void onDestroy(Fragment fragment) {

    }

    @Override
    public void onDetach(Fragment fragment) {

    }

    @Override
    public void onDestroyView(Fragment fragment) {

    }
}
