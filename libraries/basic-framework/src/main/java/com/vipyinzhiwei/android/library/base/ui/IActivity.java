package com.vipyinzhiwei.android.library.base.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * 横向切入的Activity生命周期方法管理类，所有方法与Activity生命周期方法一一对应。目前仅提供给友盟统计分析使用。后续根据业务需求扩展。
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public interface IActivity {

    /**
     * 提供给友盟推送使用
     *
     * @param activity
     */
    void onCreate(Activity activity, Bundle savedInstanceState);

    void onNewIntent(Activity activity, Intent intent);

    void onRestart(Activity activity);

    void onStart(Activity activity);

    /**
     * 提供给友盟统计使用
     *
     * @param activity
     */
    void onResume(Activity activity);

    /**
     * 提供给友盟统计使用
     *
     * @param activity
     */
    void onPause(Activity activity);

    void onStop(Activity activity);

    void onDestroy(Activity activity);

    /**
     * 提供给友盟分享使用
     *
     * @param activity
     * @param requestCode
     * @param resultCode
     * @param data
     */
    void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data);
}
