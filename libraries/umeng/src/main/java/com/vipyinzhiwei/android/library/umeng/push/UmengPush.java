package com.vipyinzhiwei.android.library.umeng.push;

import android.app.Notification;
import android.content.Context;
import android.util.Log;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengCallback;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.MsgConstant;
import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.UmengNotificationClickHandler;
import com.umeng.message.common.inter.ITagManager;
import com.umeng.message.entity.UMessage;
import com.umeng.message.tag.TagManager;
import com.vipyinzhiwei.android.library.umeng.push.notification.UmengNotificationService;

import java.util.Hashtable;

/**
 * 友盟推送工具类封装
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class UmengPush {

    private static final String TAG = "UmengPush";

    /**
     * 如果您已经在AndroidManifest.xml中配置过appkey和channel值，可以调用此版本初始化函数
     *
     * @param mContext   当前宿主进程的 ApplicationContext 上下文，不能为空
     * @param deviceType 设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
     * @param pushSecret Push推送业务的secret
     */
    public static void initSDK(Context mContext, int deviceType, String pushSecret, boolean isDebug) {
        initSDK(mContext, null, null, deviceType, pushSecret, isDebug);
    }

    /**
     * 初始化及通用接口（common库）
     *
     * @param context    当前宿主进程的 ApplicationContext 上下文，不能为空
     * @param appkey     【友盟+】 AppKey，非必须参数，如果 Manifest 文件中已配置 AppKey，该参数可以传空，则使用 Manifest 中配置的 AppKey，否则该参数必须传入。
     * @param channel    【友盟+】 Channel，非必须参数，如果 Manifest 文件中已配置 Channel，该参数可以传空，则使用 Manifest 中配置的 Channel，否则该参数必须传入
     * @param deviceType 设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
     * @param pushSecret Push 推送业务的 secret，需要集成 Push 功能时必须传入 Push 的 secret，否则传空。
     */
    public static void initSDK(Context context, String appkey, String channel, int deviceType, String pushSecret, boolean isDebug) {
        UMConfigure.init(context, appkey, channel, deviceType, pushSecret);
        config(context, isDebug);
    }

    /**
     * 友盟推送配置
     *
     * @param context 上下文
     * @param isDebug 当前运行环境
     */
    public static void config(Context context, boolean isDebug) {
        config(context, isDebug, null, null);
    }

    /**
     * 友盟推送配置
     *
     * @param context             上下文
     * @param isDebug             当前运行环境
     * @param nftclkhandler       自定义通知打开动作监听
     * @param umengMessageHandler 控制通知展示、样式、消息处理等监听
     */
    public static void config(Context context, boolean isDebug, UmengNotificationClickHandler
            nftclkhandler, UmengMessageHandler umengMessageHandler) {
        final PushAgent mPushAgent = PushAgent.getInstance(context);

        //通知免打扰模式
        mPushAgent.setNoDisturbMode(0, 0, 0, 0);

        //设置通知冷却时间，默认情况下，同一台设备在 1 分钟内收到同一个应用的多条通知时，不会重复提醒，同时在通知栏里新的通知会替换掉旧的通知。
        mPushAgent.setMuteDurationSeconds(60);

        //通知栏按数量显示，通知栏可以设置最多显示通知的条数，当有新通知到达时，会把旧的通知隐藏。参数0~10之间。当参数为 0 时，表示不合并通知。
        mPushAgent.setDisplayNotificationNumber(0);

        /**
         * 客户端控制通知到达响铃、震动、呼吸灯。（Android 8.0 及以上版本系统，需开发者重写 UmengMessageHandler 的 getNotification
         * 方法，使用 NotificationChannel 类来控制。）
         * 有三种状态：服务端控制、客户端允许、客户端禁止
         * 1.服务端控制：通过服务端推送状态来设置客户端响铃、震动、呼吸灯的状态
         * 2.客户端允许：不关心服务端推送状态，客户端都会响铃、震动、呼吸灯亮
         * 3.客户端禁止：不关心服务端推送状态，客户端不会响铃、震动、呼吸灯亮**/
        mPushAgent.setNotificationPlaySound(MsgConstant.NOTIFICATION_PLAY_SERVER); //服务端控制

        //自定义通知栏图标
        // 1.如果在发送后台没有指定通知栏图标，2.SDK 将使用本地的默认图标，在drawable目录下，放置两张图片

        //自定义通知栏声音（自定义通知栏声音仅在 Android 8.0 以下机型生效，如需适配 Android 8.0 及以上版本，请参考高级功能 - 自定义通知栏样式，重写
        // getNotification 方法，设置声音。）
        // 1.如果在发送后台没有指定通知栏的声音，SDK 将采用本地默认的声音，即在res/raw/下的文件。若无此文件，2.则默认使用系统的 Notification 声音。

        //应用在前台时否显示通知。此方法请在mPushAgent.register方法之前调用。
        mPushAgent.setNotificaitonOnForeground(false);

        //自定义资源包名
        //mPushAgent.setResourcePackageName(String packageName);

        //设置是否检查集成配置文件，可以自行检查开发者的配置问题。SDK 默认不检查集成配置文件。
        mPushAgent.setPushCheck(isDebug);

        //自定义通知打开动作监听
        mPushAgent.setNotificationClickHandler(nftclkhandler);

        //控制通知展示、样式、消息处理等监听
        mPushAgent.setMessageHandler(umengMessageHandler);

        /**
         * 自定义参数，有两种方法获取友盟Push SDK 发送至客户端的自定义参数。
         *
         * 方法一：开发者可以通过 SDK 提供的回调方法来获取自定义参数。例如，重写UmengMessageHandler类中的getNotification(Context
         * context, UMessage msg)方法
         * 也可重写UmengNotificationClickHandler类中的dealWithCustomAction(Context context, UMessage
         * msg)方法。
         *
         * 方法二：若开发使用 Push SDK 默认处理通知消息，则自定义参数将会通过Intent传递给相应的Activity。
         */
    }

    /**
     * 注册推送服务，务必在工程的自定义 Application 类的 onCreate() 方法中注册推送服务，无论推送是否开启都需要调用此方法
     *
     * @param context
     */
    public static void registerPush(Context context) {
        //注册推送服务，每次调用register方法都会回调该接口
        PushAgent.getInstance(context).register(new IUmengRegisterCallback() {

            @Override
            public void onSuccess(String deviceToken) {
                //注册成功会返回device token
                Log.i(TAG, "register onSuccess deviceToken：" + deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.w(TAG, "register onFailure: " + s1);
            }
        });
    }

    /**
     * 注册推送服务，务必在工程的自定义 Application 类的 onCreate() 方法中注册推送服务，无论推送是否开启都需要调用此方法
     *
     * @param context
     */
    public static void registerPush(Context context, IUmengRegisterCallback callback) {
        //注册推送服务，每次调用register方法都会回调该接口
        PushAgent.getInstance(context).register(callback);
    }

    /**
     * 添加标签 (Tag),目前每个用户 tag 限制在 1024 个， 每个 tag 最大 128 字符。
     *
     * @param context
     * @param tags
     */
    public static void addTags(Context context, String... tags) {
        //在原有标签基础上增加新标签。
        PushAgent.getInstance(context).getTagManager().addTags(new TagManager.TCallBack() {

            @Override
            public void onMessage(final boolean isSuccess, final ITagManager.Result result) {
                //isSuccess表示操作是否成功
            }

        }, tags);
    }

    /**
     * 添加标签 (Tag),目前每个用户 tag 限制在 1024 个， 每个 tag 最大 128 字符。
     *
     * @param context
     * @param tCallBack
     * @param tags
     */
    public static void addTags(Context context, TagManager.TCallBack tCallBack, String... tags) {
        //在原有标签基础上增加新标签。
        PushAgent.getInstance(context).getTagManager().addTags(tCallBack, tags);
    }

    /**
     * 删除标签
     *
     * @param context
     * @param tags
     */
    public static void deleteTags(Context context, String... tags) {
        //将之前添加的标签中的一个或多个删除。
        PushAgent.getInstance(context).getTagManager().deleteTags(new TagManager.TCallBack() {

            @Override
            public void onMessage(final boolean isSuccess, final ITagManager.Result result) {
                Log.i(TAG, "deleteTags onMessage：" + isSuccess);
            }

        }, tags);
    }

    /**
     * 删除标签
     *
     * @param context
     * @param tCallBack
     * @param tags
     */
    public static void deleteTags(Context context, TagManager.TCallBack tCallBack, String... tags) {
        //将之前添加的标签中的一个或多个删除。
        PushAgent.getInstance(context).getTagManager().deleteTags(tCallBack, tags);
    }

    /**
     * 获取服务器端的所有标签
     */
    public static void getTags(Context context, TagManager.TagListCallBack var1) {
        PushAgent.getInstance(context).getTagManager().getTags(var1);
    }

    /**
     * 添加加权标签，在原有标签基础上增加新加权标签，权值（value）需大于等于-10，小于等于10
     *
     * @param context
     * @param hashtable
     */
    public void addWeightedTags(Context context, Hashtable<String, Integer> hashtable) {
        PushAgent.getInstance(context).getTagManager().addWeightedTags(new TagManager.TCallBack() {
            @Override
            public void onMessage(final boolean isSuccess, final ITagManager.Result result) {
                Log.i(TAG, "addWeightedTags onMessage：" + isSuccess);
            }
        }, hashtable);
    }

    /**
     * 添加加权标签，在原有标签基础上增加新加权标签，权值（value）需大于等于-10，小于等于10
     *
     * @param context
     * @param callBack
     * @param hashtable
     */
    public void addWeightedTags(Context context, TagManager.TCallBack callBack, Hashtable<String,
            Integer> hashtable) {
        PushAgent.getInstance(context).getTagManager().addWeightedTags(callBack, hashtable);
    }

    /**
     * 删除加权标签，将之前添加的加权标签中的一个或多个删除。
     *
     * @param context
     * @param tags
     */
    public void deleteWeightedTags(Context context, final String... tags) {
        PushAgent.getInstance(context).getTagManager().deleteWeightedTags(new TagManager
                .TCallBack() {

            @Override
            public void onMessage(final boolean isSuccess, final ITagManager.Result result) {
                Log.i(TAG, "deleteWeightedTags onMessage：" + isSuccess);
            }

        }, tags);
    }

    /**
     * 删除加权标签，将之前添加的加权标签中的一个或多个删除。
     *
     * @param context
     * @param callBack
     * @param tags
     */
    public void deleteWeightedTags(Context context, final TagManager.TCallBack callBack, final
    String... tags) {
        PushAgent.getInstance(context).getTagManager().deleteWeightedTags(callBack, tags);
    }

    /**
     * 获取服务器端的所有加权标签
     *
     * @param callBack
     */
    public void getWeightedTags(Context context, TagManager.WeightedTagListCallBack callBack) {
        PushAgent.getInstance(context).getTagManager().getWeightedTags(callBack);
    }

    /**
     * 设置用户别名（Alias），设置用户 id 和 device_token 的一对多的映射关系
     */
    public static void addAlias(Context context, String var1, String var2) {
        PushAgent.getInstance(context).addAlias(var1, var2, new UTrack.ICallBack() {

            @Override
            public void onMessage(boolean isSuccess, String message) {
                Log.i(TAG, "addAlias onMessage：" + isSuccess);
            }
        });
    }

    /**
     * 设置用户别名（Alias），设置用户 id 和 device_token 的一一映射关系，确保同一个 alias 只对应一台设备
     */
    public static void setAlias(Context context, String var1, String var2) {
        PushAgent.getInstance(context).setAlias(var1, var2, new UTrack.ICallBack() {

            @Override
            public void onMessage(boolean isSuccess, String message) {
                Log.i(TAG, "setAlias onMessage：" + isSuccess);
            }
        });
    }

    /**
     * 设置用户别名（Alias），移除用户 id，若要使用新的 alias，请先调用deleteAlias接口移除掉旧的 alias，再调用addAlias添加新的 alias
     */
    public static void deleteAlias(Context context, String var1, String var2) {
        PushAgent.getInstance(context).setAlias(var1, var2, new UTrack.ICallBack() {

            @Override
            public void onMessage(boolean isSuccess, String message) {
                Log.i(TAG, "deleteAlias onMessage：" + isSuccess);
            }
        });
    }

    /**
     * 完全自定义处理（透传），如果使用了完全自定义处理后，又想恢复成【友盟 +】的默认的处理，参数直接传入null即可。
     */
    public static void setPushIntentServiceClass(Context context, Class<? extends
            UmengNotificationService> notificationService) {
        PushAgent.getInstance(context).setPushIntentServiceClass(notificationService);
    }


    //关闭推送（请在 Activity 内调用）
    public static void disable(Context context) {
        PushAgent.getInstance(context).disable(new IUmengCallback() {

            @Override
            public void onSuccess() {
                Log.i(TAG, "disable onSuccess");
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.w(TAG, "disable onFailures");
            }

        });
    }

    //开启推送（请在 Activity 内调用）
    public static void enable(Context context) {
        PushAgent.getInstance(context).disable(new IUmengCallback() {

            @Override
            public void onSuccess() {
                Log.i(TAG, "disable onSuccess");
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.w(TAG, "disable onFailures");
            }

        });
    }


    /*--------------------------仅供参考说明，具体业务实现由客户端实现 start--------------------------*/

    UmengNotificationClickHandler clickHandler = new UmengNotificationClickHandler() {
        /**
         * 自定义通知打开动作
         * @param context
         * @param uMessage
         */
        @Override
        public void dealWithCustomAction(Context context, UMessage uMessage) {
            super.dealWithCustomAction(context, uMessage);
            //若需启动Activity需为Intent添加Flag：Intent.FLAG_ACTIVITY_NEW_TASK，否则无法启动 Activity。
        }
    };

    /**
     * 仅供参考，具体业务实现由客户端实现
     */
    UmengMessageHandler umengMessageHandler = new UmengMessageHandler() {
        /**
         * 通知的回调方法，控制通知是否展示
         * @param context
         * @param uMessage
         */
        @Override
        public void dealWithNotificationMessage(Context context, UMessage uMessage) {
            //调用super则会走通知展示流程，不调用super则不展示通知
            super.dealWithNotificationMessage(context, uMessage);
        }

        /**
         * 自定义通知栏样式
         * @param context
         * @param uMessage
         * @return
         */
        @Override
        public Notification getNotification(Context context, UMessage uMessage) {
            return super.getNotification(context, uMessage);
        }

        /**
         * 自定义消息处理（处理全部消息内容，包含自定义参数）
         * @param context
         * @param uMessage
         */
        @Override
        public void dealWithCustomMessage(Context context, UMessage uMessage) {
            super.dealWithCustomMessage(context, uMessage);
        }
    };

    /*--------------------------仅供参考说明，具体业务实现由客户端实现 end----------------------------*/
}
