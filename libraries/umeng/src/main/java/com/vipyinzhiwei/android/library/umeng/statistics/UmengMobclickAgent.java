package com.vipyinzhiwei.android.library.umeng.statistics;

import android.content.Context;

import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.util.Map;

/**
 * 统计的常用几种场景:
 * <p>
 * 第1种：如果页面是直接由 Activity 实现的，统计代码大约是这样：
 * public void onResume() {
 * super.onResume();
 * MobclickAgent.onPageStart("SplashScreen"); //手动统计页面("SplashScreen"为页面名称，可自定义)
 * MobclickAgent.onResume(this); //统计时长
 * }
 * <p>
 * public void onPause() {
 * super.onPause();
 * MobclickAgent.onPageEnd("SplashScreen"); //手动统计页面("SplashScreen"为页面名称，可自定义)，必须保证 onPageEnd 在 onPause 之前调用，因为SDK会在 onPause 中保存onPageEnd统计到的页面数据。
 * MobclickAgent.onPause(this);
 * }
 * <p>
 * <p>
 * 第2种：如果页面是使用 FragmentActivity + Fragment 实现的，需要在 FragmentActivity 中统计时长：
 * public void onResume() {
 * super.onResume();
 * MobclickAgent.onResume(this); //统计时长
 * }
 * public void onPause() {
 * super.onPause();
 * MobclickAgent.onPause(this); //统计时长
 * }
 * <p>
 * 并在其包含的 Fragment 中统计页面：
 * public void onResume() {
 * super.onResume();
 * MobclickAgent.onPageStart("MainScreen"); //统计页面("MainScreen"为页面名称，可自定义)
 * }
 * public void onPause() {
 * super.onPause();
 * MobclickAgent.onPageEnd("MainScreen");
 * }
 */


/**
 * 友盟统计工具类封装
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class UmengMobclickAgent {

    /**
     * 如果您已经在AndroidManifest.xml中配置过appkey和channel值，可以调用此版本初始化函数
     *
     * @param mContext   当前宿主进程的 ApplicationContext 上下文，不能为空
     * @param deviceType 设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
     * @param pushSecret Push推送业务的secret
     */
    public static void initSDK(Context mContext, int deviceType, String pushSecret, boolean isDebug) {
        UMConfigure.init(mContext, null, null, deviceType, pushSecret);
    }

    /**
     * 初始化及通用接口（common库）
     *
     * @param context    当前宿主进程的 ApplicationContext 上下文，不能为空
     * @param appkey     【友盟+】 AppKey，非必须参数，如果 Manifest 文件中已配置 AppKey，该参数可以传空，则使用 Manifest 中配置的 AppKey，否则该参数必须传入。
     * @param channel    【友盟+】 Channel，非必须参数，如果 Manifest 文件中已配置 Channel，该参数可以传空，则使用 Manifest 中配置的 Channel，否则该参数必须传入
     * @param deviceType 设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
     * @param pushSecret Push 推送业务的 secret，需要集成 Push 功能时必须传入 Push 的 secret，否则传空。
     */
    public static void initSDKAndConfig(Context context, String appkey, String channel, int deviceType, String pushSecret, boolean isDebug) {
        UMConfigure.init(context, appkey, channel, deviceType, pushSecret);
        config(context, isDebug);
    }

    /**
     * 定制友盟统计相关配置
     *
     * @param mContext
     * @param isDebug  true=debug模式
     */
    public static void config(Context mContext, boolean isDebug) {
        //设置场景类型。EScenarioType.E_UM_NORMAL 普通统计场景，EScenarioType.E_UM_GAME 游戏场景
        MobclickAgent.setScenarioType(mContext, MobclickAgent.EScenarioType.E_UM_NORMAL);
        //只捕获线上异常信息
        MobclickAgent.setCatchUncaughtExceptions(isDebug);
        //禁止默认的页面统计功能，这样将不会再自动统计 Activity 页面。
        MobclickAgent.openActivityDurationTrack(false);
        //设置 secretkey。防止 AppKey 被盗用，secretkey 需要企业认证，网站申请。申请方法见： http://dev.umeng.com/analytics/reports/secretkey-detail
        //MobclickAgent.setSecret(mContext, "");
    }

    /**
     * 自定义事件统计-计数事件
     *
     * @param context 当前宿主进程的 ApplicationContext 上下文。
     * @param eventID 自定义事件 id
     */
    public static void onEvent(Context context, String eventID) {
        MobclickAgent.onEvent(context, eventID);
    }

    /**
     * 自定义事件统计-计数事件
     *
     * @param context 当前宿主进程的 ApplicationContext 上下文。
     * @param eventID 为当前统计的事件 ID。
     * @param label   事件的标签属性
     */
    public static void onEvent(Context context, String eventID, String label) {
        MobclickAgent.onEvent(context, eventID, label);
    }

    /**
     * 自定义事件统计-计数事件，统计点击行为各属性被触发的次数，考虑事件在不同属性上的取值，可以调用如下方法：
     *
     * @param context 当前宿主进程的 ApplicationContext 上下文。
     * @param eventID 为当前统计的事件 ID。
     * @param map     为当前事件的属性和取值（Key-Value 键值对）。
     */
    public static void onEvent(Context context, String eventID, Map<String, String> map) {
        MobclickAgent.onEvent(context, eventID, map);
    }

    /**
     * 自定义事件统计-计算事件
     *
     * @param context 当前宿主进程的 ApplicationContext 上下文。
     * @param eventID 为当前统计的事件 ID。
     * @param map     为当前事件的属性和取值（Key-Value 键值对）。
     * @param du      当前事件的数值，取值范围是 - 2,147,483,648 到 +2,147,483,647 之间的
     *                有符号整数，即 int 32 类型，如果数据超出了该范围，会造成数据丢包，
     *                影响数据统计的准确性。
     */
    public static void onEventValue(Context context, String eventID, Map<String, String> map, int du) {
        MobclickAgent.onEventValue(context, eventID, map, du);
    }


    /**
     * 手动上传捕获的错误到【友盟 +】服务器
     *
     * @param context 当前宿主进程的 ApplicationContext 上下文。
     * @param error   错误内容字符串。
     */
    public static void reportError(Context context, String error) {
        MobclickAgent.reportError(context, error);
    }

    /**
     * 手动上传捕获的错误到【友盟 +】服务器
     *
     * @param context 当前宿主进程的 ApplicationContext 上下文。
     * @param e       错误发生时抛出的异常对象。
     */
    public static void reportError(Context context, Throwable e) {
        MobclickAgent.reportError(context, e);
    }

    /**
     * 程序退出时，用于保存统计数据的 API。如果开发者调用 kill 或者 exit 之类的方法杀死进程，请务必在此之前调用 onKillProcess 方法，用来保存统计数据。
     *
     * @param context 当前宿主进程的 ApplicationContext 上下文。
     */
    public static void onKillProcess(Context context) {
        MobclickAgent.onKillProcess(context);
    }
}
