package com.vipyinzhiwei.android.library.umeng.push.notification;

import android.content.Context;
import android.content.Intent;

import com.umeng.message.UmengMessageService;

import org.android.agoo.common.AgooConstants;

/**
 * 友盟推送，完全自定义处理通知（透传）
 * <p>
 * 若开发者需要实现对消息的完全自定义处理，则可以继承 UmengMessageService, 实现自己的 Service
 * 来完全控制达到消息的处理。使用完全自定义处理后，PushSDK只负责下发消息体且只统计送达数，展示逻辑需由开发者自己写代码实现，点击数和忽略数需由开发者调用UTrack
 * 类的trackMsgClick和trackMsgDismissed方法进行统计。1、实现一个类，继承 UmengMessageService， 重写onMessage(Context
 * context, Intent intent) 方法
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public abstract class UmengNotificationService extends UmengMessageService {

    protected abstract Class<? extends NotificationService> getCustomNotificationService();

    @Override
    public void onMessage(Context context, Intent intent) {
        String message = intent.getStringExtra(AgooConstants.MESSAGE_BODY);
        Intent intent1 = new Intent();
        intent1.setClass(context, getCustomNotificationService());
        intent1.putExtra("UmengMsg", message);
        context.startService(intent1);
    }
}
