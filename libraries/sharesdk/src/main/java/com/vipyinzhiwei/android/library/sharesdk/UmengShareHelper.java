package com.vipyinzhiwei.android.library.sharesdk;

import android.app.Activity;
import android.content.Context;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMEmoji;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMMin;
import com.umeng.socialize.media.UMVideo;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.media.UMusic;
import com.umeng.socialize.shareboard.ShareBoardConfig;
import com.umeng.socialize.utils.ShareBoardlistener;

import java.util.ArrayList;
import java.util.List;

/**
 * ShareSDK分享帮助类
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class UmengShareHelper {

    private static UmengShareHelper instance = null;
    private static SharePlatformActionListener mShareListener;

    private UmengShareHelper() {

    }


    public static UmengShareHelper getInstance() {
//        if (instance == null) {
//            synchronized (UmengShareHelper.class){
        instance = new UmengShareHelper();
//            }
//        }
        return instance;
    }

    /**
     * 友盟分享，初始化及通用接口（common库）
     * 注意，这里与友盟统计、推送同为一个初始化接口，不需要重复调用
     *
     * @param context    当前宿主进程的 ApplicationContext 上下文，不能为空
     * @param appkey     【友盟+】 AppKey，非必须参数，如果 Manifest 文件中已配置 AppKey，该参数可以传空，则使用 Manifest 中配置的
     *                   AppKey，否则该参数必须传入。
     * @param channel    【友盟+】 Channel，非必须参数，如果 Manifest 文件中已配置 Channel，该参数可以传空，则使用 Manifest 中配置的
     *                   Channel，否则该参数必须传入
     * @param deviceType 设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
     * @param pushSecret Push 推送业务的 secret，需要集成 Push 功能时必须传入 Push 的 secret，否则传空。
     */
    public static void initSDK(Context context, String appkey, String channel, int deviceType,
                               String pushSecret) {
        UMConfigure.init(context, appkey, channel, deviceType, pushSecret);
    }

    /**
     * 断开友盟SDK相关连接通道，断开之后不能使用分享API，需要再次initSDK
     */
    public static void stopSDK(Context context) {
        UMShareAPI.get(context).release();
        instance = null;
        mShareListener = null;
    }

    /**
     * 注册分享平台,在UmengShareHelper.initSDK方法之后调用，Application注册即可
     */
    private void registerSharePlatform() {
        PlatformConfig.setWeixin("", "");
        PlatformConfig.setSinaWeibo("", "", "");
        PlatformConfig.setQQZone("", "");
    }

    /**
     * 打开分享面板（纯文本）
     *
     * @param act      触发对话框的Activity
     * @param text     分享纯文本内容
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, String text, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withText(text), platform);
    }

    /**
     * 打开分享面板（图片）
     *
     * @param act      触发对话框的Activity
     * @param media    分享图片
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, UMImage media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withMedia(media), platform);
    }

    /**
     * 打开分享面板（图片）
     *
     * @param act      触发对话框的Activity
     * @param text     分享内容
     * @param media    分享图片
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, String text, UMImage media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withText(text).withMedia(media), platform);
    }

    /**
     * 打开分享面板（链接）
     *
     * @param act      触发对话框的Activity
     * @param media    分享链接
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, UMWeb media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withMedia(media), platform);
    }

    /**
     * 打开分享面板（链接）
     *
     * @param act      触发对话框的Activity
     * @param text     分享内容
     * @param media    分享链接
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, String text, UMWeb media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withText(text).withMedia(media), platform);
    }

    /**
     * 打开分享面板（视频）
     *
     * @param act      触发对话框的Activity
     * @param media    分享视频
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, UMVideo media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withMedia(media), platform);
    }

    /**
     * 打开分享面板（视频）
     *
     * @param act      触发对话框的Activity
     * @param text     分享内容
     * @param media    分享视频
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, String text, UMVideo media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withText(text).withMedia(media), platform);
    }

    /**
     * 打开分享面板（音乐）
     *
     * @param act      触发对话框的Activity
     * @param media    分享音乐
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, UMusic media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withMedia(media), platform);
    }

    /**
     * 打开分享面板（音乐）
     *
     * @param act      触发对话框的Activity
     * @param text     分享内容
     * @param media    分享音乐
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, String text, UMusic media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withText(text).withMedia(media), platform);
    }

    /**
     * 打开分享面板（Gif）
     *
     * @param act      触发对话框的Activity
     * @param media    分享GIF（目前只有微信好友分享支持 Emoji 表情，其他平台暂不支持）
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间）平台
     */
    public void openSharePannel(Activity act, UMEmoji media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withMedia(media), platform);
    }

    /**
     * 打开分享面板（Gif）
     *
     * @param act      触发对话框的Activity
     * @param text     分享内容
     * @param media    分享GIF（目前只有微信好友分享支持 Emoji 表情，其他平台暂不支持）
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间）平台
     */
    public void openSharePannel(Activity act, String text, UMEmoji media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withText(text).withMedia(media), platform);
    }

    /**
     * 打开分享面板（小程序）
     *
     * @param act      触发对话框的Activity
     * @param media    分享微信小程序
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, UMMin media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withMedia(media), platform);
    }

    /**
     * 打开分享面板（小程序）
     *
     * @param act      触发对话框的Activity
     * @param text     分享内容
     * @param media    分享微信小程序
     * @param platform 分享平台（不传入参数，默认分享到（微信、朋友圈、QQ、QQ空间、微博）平台
     */
    public void openSharePannel(Activity act, String text, UMMin media, SHARE_MEDIA... platform) {
        open(new ShareAction(act).withText(text).withMedia(media), platform);
    }

    /**
     * 打开分享面板
     *
     * @param shareAction
     * @param platform
     */
    private void open(ShareAction shareAction, SHARE_MEDIA... platform) {
        if (platform == null || platform.length == 0) {
            //不传入平台参数，默认打开分享面板，微信、朋友圈、QQ、QQ空间、微博
            shareAction.setDisplayList(SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA
                    .QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.SINA, SHARE_MEDIA.SMS);
        } else {
            shareAction.setDisplayList(platform);
        }
        shareAction.setCallback(mInnerShareListener).open();
    }

    /**
     * 分享监听器
     */
    private SharePlatformActionListener mInnerShareListener = new SharePlatformActionListener() {

        @Override
        public void onShareStart(SHARE_MEDIA platform) {
            if (mShareListener != null) {
                mShareListener.onStart(platform);
            }
        }

        @Override
        public void onShareSuccess(SHARE_MEDIA platform) {
            if (mShareListener != null) {
                mShareListener.onShareSuccess(platform);
            }
        }

        @Override
        public void onShareFailure(SHARE_MEDIA platform, Throwable t) {
            if (mShareListener != null) {
                mShareListener.onShareFailure(platform, t);
            }
        }

        @Override
        public void onShareCancel(SHARE_MEDIA platform) {
            if (mShareListener != null) {
                mShareListener.onShareCancel(platform);
            }
        }
    };

    public void setShareListener(SharePlatformActionListener mShareListener) {
        this.mShareListener = mShareListener;
    }

    /**
     * 将字符串内容拼接为UMWeb对象
     *
     * @param linkURL     url
     * @param title       标题
     * @param thumb       缩略图
     * @param description 描述
     * @return UMWeb
     */
    public static UMWeb targetToUMWeb(String linkURL, String title, UMImage thumb, String
            description) {
        UMWeb web = new UMWeb(linkURL);
        web.setTitle(title);
        web.setThumb(thumb);
        web.setDescription(description);
        return web;
    }

    /**
     * 将字符串内容拼接为UMImage对象
     *
     * @param imageurl 网络图片url
     * @return UMImage
     */
    public static UMImage targetToUMImage(Context context, String imageurl) {
        return new UMImage(context, imageurl);
    }

    /**
     * 将字符串内容拼接为UMImage对象
     *
     * @param resId 图片资源文件
     * @return UMImage
     */
    public static UMImage targetToUMImage(Context context, int resId) {
        return new UMImage(context, resId);
    }

    /**
     * 分享平台转换为友盟支持的分享平台数据结构
     *
     * @param sharePlatform
     * @return
     */
    private List<SHARE_MEDIA> getSharePlatforms(List<String> sharePlatform) {
        List<SHARE_MEDIA> shareMedias = new ArrayList<>();
        if (sharePlatform == null || sharePlatform.isEmpty()) {
            shareMedias.add(SHARE_MEDIA.WEIXIN_CIRCLE);
            shareMedias.add(SHARE_MEDIA.WEIXIN);
            shareMedias.add(SHARE_MEDIA.QQ);
            shareMedias.add(SHARE_MEDIA.QZONE);
            shareMedias.add(SHARE_MEDIA.SINA);
            shareMedias.add(SHARE_MEDIA.SMS);
        } else {
            for (String platform : sharePlatform) {
                if ("0".equals(platform)) {
                    //0-微信朋友圈x
                    shareMedias.add(SHARE_MEDIA.WEIXIN_CIRCLE);
                } else if ("1".equals(platform)) {
                    //1-微信好友
                    shareMedias.add(SHARE_MEDIA.WEIXIN);
                } else if ("2".equals(platform)) {
                    //2-新浪微博
                    shareMedias.add(SHARE_MEDIA.SINA);
                } else if ("3".equals(platform)) {
                    //3-短信
                    shareMedias.add(SHARE_MEDIA.SINA);
                } else if ("4".equals(platform)) {
                    //4-qq好友
                    shareMedias.add(SHARE_MEDIA.QQ);
                } else if ("5".equals(platform)) {
                    //5-qq空间
                    shareMedias.add(SHARE_MEDIA.QZONE);
                }
            }
        }

        return shareMedias;
    }

}
