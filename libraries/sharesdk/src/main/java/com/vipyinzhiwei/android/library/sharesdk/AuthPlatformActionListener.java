package com.vipyinzhiwei.android.library.sharesdk;

import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.Map;

/**
 * 平台授权行为监听器
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public abstract class AuthPlatformActionListener implements UMAuthListener {

    /**
     * 授权开始的回调
     *
     * @param platform 平台名称
     */
    @Override
    public void onStart(SHARE_MEDIA platform) {
        onAuthStart(platform);
    }

    /**
     * 授权成功的回调
     *
     * @param platform 平台名称
     * @param action   行为序号，开发者用不上
     * @param data     用户资料返回
     */
    @Override
    public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
        onAuthSuccess(platform, action, data);
    }

    /**
     * 授权失败的回调
     *
     * @param platform 平台名称
     * @param action   行为序号，开发者用不上
     * @param t        错误原因
     */
    @Override
    public void onError(SHARE_MEDIA platform, int action, Throwable t) {
        onAuthFailure(platform, action, t);
    }

    /**
     * 授权取消的回调
     *
     * @param platform 平台名称
     * @param action   行为序号，开发者用不上
     */
    @Override
    public void onCancel(SHARE_MEDIA platform, int action) {
        onAuthCancel(platform, action);
    }


    /**
     * 授权开始的回调
     *
     * @param platform 平台类型
     */
    public abstract void onAuthStart(SHARE_MEDIA platform);

    /**
     * 授权成功的回调
     *
     * @param platform 平台类型
     */
    public abstract void onAuthSuccess(SHARE_MEDIA platform, int action, Map<String, String> data);

    /**
     * 授权失败的回调
     *
     * @param platform 平台类型
     * @param t        错误原因
     */
    public abstract void onAuthFailure(SHARE_MEDIA platform, int action, Throwable t);

    /**
     * 授权取消的回调
     *
     * @param platform 平台类型
     */
    public abstract void onAuthCancel(SHARE_MEDIA platform, int action);

    /**
     * 授权面板点击回调
     *
     * @param platform 分享平台
     */
    public abstract void onItemClick(SHARE_MEDIA platform);
}
