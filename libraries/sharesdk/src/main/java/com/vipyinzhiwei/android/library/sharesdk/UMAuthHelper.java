package com.vipyinzhiwei.android.library.sharesdk;

import android.app.Activity;

import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareConfig;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.Map;

/**
 * 友盟第三方登录（授权）帮助类
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class UMAuthHelper {

    private AuthPlatformActionListener authPlatformActionListener;

    private static UMAuthHelper instance = null;

    private UMAuthHelper() {

    }

    public static UMAuthHelper instance() {
        if (instance == null) {
//            synchronized (UMAuthHelper.class){
            instance = new UMAuthHelper();
//            }
//        }
        }

        return instance;
    }

    /**
     * 设置每次登录拉取确认界面，目前 SDK 默认设置为在 Token 有效期内登录不进行二次授权，
     * 如果有需要每次登录都弹出授权页面，便于切换账号的开发者可以添加上述代码
     *
     * @param activity
     */
    public static void setIsNeedAuthOnGetUserInfo(Activity activity) {
        UMShareConfig config = new UMShareConfig();
        config.isNeedAuthOnGetUserInfo(true);
        UMShareAPI.get(activity).setShareConfig(config);
    }

    /**
     * 获取用户资料（请求授权）
     */
    public void getPlatformInfo(Activity context, SHARE_MEDIA shareMedia) {
        UMShareAPI.get(context).getPlatformInfo(context, shareMedia, mAuthListener);
    }

    /**
     * 删除授权
     */
    public void deleteOauth(Activity context, SHARE_MEDIA shareMedia) {
        UMShareAPI.get(context).deleteOauth(context, shareMedia, mAuthListener);
    }


    /**
     * 授权回调监听
     */
    private UMAuthListener mAuthListener = new UMAuthListener() {
        /**
         * 授权开始的回调
         *
         * @param platform 平台名称
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            if (authPlatformActionListener != null) {
                authPlatformActionListener.onStart(platform);
            }
        }

        /**
         * 授权成功的回调
         *
         * @param platform  平台名称
         * @param action    行为序号，开发者用不上
         * @param data      用户资料返回
         */
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            if (authPlatformActionListener != null) {
                authPlatformActionListener.onAuthSuccess(platform, action, data);
            }
        }

        /**
         * 授权失败的回调
         *
         * @param platform  平台名称
         * @param action    行为序号，开发者用不上
         * @param t         错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            if (authPlatformActionListener != null) {
                authPlatformActionListener.onAuthFailure(platform, action, t);
            }
        }

        /**
         * 授权取消的回调
         *
         * @param platform  平台名称
         * @param action    行为序号，开发者用不上
         */
        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            if (authPlatformActionListener != null) {
                authPlatformActionListener.onAuthCancel(platform, action);
            }
        }
    };

    public void setAuthPlatformActionListener(AuthPlatformActionListener
                                                      authPlatformActionListener) {
        this.authPlatformActionListener = authPlatformActionListener;
    }
}
