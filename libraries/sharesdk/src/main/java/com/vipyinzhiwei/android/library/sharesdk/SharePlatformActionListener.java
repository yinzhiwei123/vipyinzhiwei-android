package com.vipyinzhiwei.android.library.sharesdk;

import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * 平台分享行为监听器
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public abstract class SharePlatformActionListener implements UMShareListener {

    /**
     * 分享开始的回调
     *
     * @param platform 平台类型
     */
    @Override
    public void onStart(SHARE_MEDIA platform) {
        onShareStart(platform);
    }

    /**
     * 分享成功的回调
     *
     * @param platform 平台类型
     */
    @Override
    public void onResult(SHARE_MEDIA platform) {
        onShareSuccess(platform);
    }

    /**
     * 分享失败的回调
     *
     * @param platform 平台类型
     * @param t        错误原因
     */
    @Override
    public void onError(SHARE_MEDIA platform, Throwable t) {
        onShareFailure(platform, t);
    }

    /**
     * 分享取消的回调
     *
     * @param platform 平台类型
     */
    @Override
    public void onCancel(SHARE_MEDIA platform) {
        onShareCancel(platform);
    }


    /**
     * 分享开始的回调
     *
     * @param platform 平台类型
     */
    public abstract void onShareStart(SHARE_MEDIA platform);

    /**
     * 分享成功的回调
     *
     * @param platform 平台类型
     */
    public abstract void onShareSuccess(SHARE_MEDIA platform);

    /**
     * 分享失败的回调
     *
     * @param platform 平台类型
     * @param t        错误原因
     */
    public abstract void onShareFailure(SHARE_MEDIA platform, Throwable t);

    /**
     * 分享取消的回调
     *
     * @param platform 平台类型
     */
    public abstract void onShareCancel(SHARE_MEDIA platform);
}
