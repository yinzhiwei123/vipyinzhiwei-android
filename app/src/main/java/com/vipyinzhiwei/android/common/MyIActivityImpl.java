package com.vipyinzhiwei.android.common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;
import com.umeng.socialize.UMShareAPI;
import com.vipyinzhiwei.android.library.base.ui.IActivity;

/**
 * 友盟统计分析使用的Activity生命周期方法管理默认实现类。
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class MyIActivityImpl implements IActivity {

    @Override
    public void onCreate(Activity activity, Bundle savedInstanceState) {
        //统计应用启动数据（此方法为友盟推送sdk里的api），此方法与统计分析sdk中统计日活的方法无关！
        PushAgent.getInstance(activity).onAppStart();
    }

    @Override
    public void onNewIntent(Activity activity, Intent intent) {

    }

    @Override
    public void onRestart(Activity activity) {

    }

    @Override
    public void onStart(Activity activity) {

    }

    @Override
    public void onResume(Activity activity) {
        //Session 启动、App 使用时长等基础数据统计接口 API
        MobclickAgent.onResume(activity);
        //手动统计页面("fragment.getClass().getName()"为页面名称，可自定义)
        MobclickAgent.onPageStart(activity.getClass().getName());
    }

    @Override
    public void onPause(Activity activity) {
        //Session 启动、App 使用时长等基础数据统计接口 API
        MobclickAgent.onPause(activity);
        //手动统计页面("fragment.getClass().getName()"为页面名称，可自定义)
        MobclickAgent.onPageEnd(activity.getClass().getName());
    }

    @Override
    public void onStop(Activity activity) {

    }

    @Override
    public void onDestroy(Activity activity) {

    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent
            data) {
        /**
         * QQ 与新浪不需要添加 Activity，但需要在使用 QQ 分享或者授权的 Activity 中，
         * 注意onActivityResult不可在 fragment 中实现，如果在fragment 中调用登录或分享，需要在 fragment 依赖的 Activity 中实现
         */
        UMShareAPI.get(activity).onActivityResult(requestCode, resultCode, data);
    }
}
