package com.vipyinzhiwei.android.common;

import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.vipyinzhiwei.android.library.base.ui.ShareActivity;
import com.vipyinzhiwei.android.library.sharesdk.SharePlatformActionListener;
import com.vipyinzhiwei.android.library.sharesdk.UmengShareHelper;

import java.util.ArrayList;

/**
 * 具备分享功能的Activity的基类
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public abstract class CommonShareActivity extends ShareActivity {
    /**
     * 分享辅助工具类
     */
    protected UmengShareHelper mShareSDKHelper;

    @Override
    protected void initShare() {
        mShareSDKHelper = UmengShareHelper.getInstance();
        mShareSDKHelper.setShareListener(new BasicShareListener());
    }

    /**
     * 打开分享对话框
     *
     * @param imageURL   分享图片地址
     * @param title      分享标题
     * @param strContent 分享内容
     * @param linkURL    点击打开的链接地址
     */
    @Override
    protected void openSharePannel(String imageURL, String title, String strContent, String
            linkURL) {
        if (null == mShareSDKHelper) return;
        // TODO: 2018/7/24 分享待优化
        mShareSDKHelper.openSharePannel(this, new UMWeb(linkURL, title, strContent, new UMImage
                (this, imageURL)));
    }

    /**
     * 打开分享对话框
     *
     * @param imageURL      分享图片地址
     * @param title         分享标题
     * @param strContent    分享内容
     * @param linkURL       点击打开的链接地址
     * @param sharePlatform 分享平台,见IBaseConstant.PLATFORM_前缀对应的平台枚举，默认全平台
     */
    @Override
    public void openSharePannel(String imageURL, String title, String strContent, String linkURL,
                                ArrayList<String> sharePlatform) {
        if (null == mShareSDKHelper) return;
        // TODO: 2018/7/24 分享待优化
        mShareSDKHelper.openSharePannel(this, new UMWeb(linkURL, title, strContent, new UMImage
                (this, imageURL)));

    }

    /**
     * 打开分享对话框
     *
     * @param imageResId    分享图片地址，工程本地地址
     * @param title         分享标题
     * @param strContent    分享内容
     * @param linkURL       点击打开的链接地址
     * @param sharePlatform 分享平台,见IBaseConstant.PLATFORM_前缀对应的平台枚举，默认全平台
     */
    @Override
    public void openSharePannel(int imageResId, String title, String strContent, String linkURL,
                                ArrayList<String> sharePlatform) {
        if (null == mShareSDKHelper) return;
        // TODO: 2018/7/24 分享待优化
        mShareSDKHelper.openSharePannel(this, new UMWeb(linkURL, title, strContent, new UMImage
                (this, imageResId)));
    }

    /**
     * 分享回调监听器
     */
    protected class BasicShareListener extends SharePlatformActionListener {


        @Override
        public void onShareStart(SHARE_MEDIA platform) {
            CommonShareActivity.this.onShareStart(platform.getName());
        }

        @Override
        public void onShareSuccess(SHARE_MEDIA platform) {
            CommonShareActivity.this.onShareSuccess(platform.getName());
        }

        @Override
        public void onShareFailure(SHARE_MEDIA platform, Throwable t) {
            CommonShareActivity.this.onShareFailure(platform.getName(), t);
        }

        @Override
        public void onShareCancel(SHARE_MEDIA platform) {
            CommonShareActivity.this.onShareCancel(platform.getName());
        }
    }
}
