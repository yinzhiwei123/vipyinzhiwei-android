package com.vipyinzhiwei.android.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.umeng.analytics.MobclickAgent;
import com.vipyinzhiwei.android.library.base.ui.IFragment;

/**
 * 友盟统计分析使用的Fragment生命周期方法管理默认实现类。
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class MyIFragmentImpl implements IFragment {

    @Override
    public void onAttach(Fragment fragment) {

    }

    @Override
    public void onCreate(Fragment fragment, Bundle savedInstanceState) {

    }

    @Override
    public void onCreateView(Fragment fragment, LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

    }

    @Override
    public void onActivityCreated(Fragment fragment, Bundle savedInstanceState) {

    }

    @Override
    public void onViewCreated(Fragment fragment, View view, Bundle savedInstanceState) {

    }

    @Override
    public void onStart(Fragment fragment) {

    }

    @Override
    public void onResume(Fragment fragment) {
        //手动统计页面("fragment.getClass().getName()"为页面名称，可自定义)
        MobclickAgent.onPageStart(fragment.getClass().getName());
    }

    @Override
    public void onPause(Fragment fragment) {
        //手动统计页面("fragment.getClass().getName()"为页面名称，可自定义)，必须保证 onPageEnd 在 onPause 之前调用，因为SDK会在
        // onPause 中保存onPageEnd统计到的页面数据。
        MobclickAgent.onPageEnd(fragment.getClass().getName());
    }

    @Override
    public void onStop(Fragment fragment) {

    }

    @Override
    public void onDestroy(Fragment fragment) {

    }

    @Override
    public void onDetach(Fragment fragment) {

    }

    @Override
    public void onDestroyView(Fragment fragment) {

    }
}
