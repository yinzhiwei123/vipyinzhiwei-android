package com.vipyinzhiwei.android.common;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.vipyinzhiwei.android.R;
import com.vipyinzhiwei.android.library.Alert;
import com.vipyinzhiwei.android.library.base.ui.CommonActivity;
import com.vipyinzhiwei.android.library.network.okhttp.OkHttpUtils;
import com.vipyinzhiwei.android.library.network.okhttp.callback.StringCallback;
import com.vipyinzhiwei.android.library.tools.ToolString;
import com.vipyinzhiwei.android.library.tools.ToolToast;

import okhttp3.Call;

/**
 * 意见反馈Activity
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class FeedbackActivity extends CommonActivity {
    private Button btn_sumbit;
    private EditText et_message;
    private final static String FEED_BACK_URL = "";

    @Override
    public int bindLayout() {
        return R.layout.activity_feedback;
    }

    @SuppressLint("NewApi")
    @Override
    public void initView(View view) {
        btn_sumbit = (Button) findViewById(R.id.btn_sumbit);
        et_message = (EditText) findViewById(R.id.et_message);

        //初始化返回按钮
        String strCenterTitle = getResources().getString(R.string.FeedbackActivity);
//      ActionBarManager.initBackTitle(getContext(), getActionBar(), strCenterTitle);
        mWindowTitle.initBackTitleBar(strCenterTitle);
        mWindowTitle.initRightDoneBtn("提交", onClickListener);

        btn_sumbit.setOnClickListener(onClickListener);
    }

    /**
     * 表单验证
     *
     * @return
     */
    private boolean validateForm() {
        String strMessage = et_message.getText().toString();
        if (ToolString.isNoBlankAndNoNull(strMessage)) {
            return true;
        } else {
            Alert.toastShort(getContext(), "请输入意见内容再提交，谢谢^_^");
            return false;
        }
    }

    /**
     * 提交按钮监听器
     */
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //提交表单
            if (validateForm()) {
                OkHttpUtils.get().url("http://www.baidu.com").build().execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ToolToast.showLong("提交失败:" + e);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ToolToast.showLong("提交成功:" + response);
                    }
                });
            }
        }
    };
}
