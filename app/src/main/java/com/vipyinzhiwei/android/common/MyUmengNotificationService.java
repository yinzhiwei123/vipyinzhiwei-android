package com.vipyinzhiwei.android.common;

import com.umeng.message.entity.UMessage;
import com.vipyinzhiwei.android.library.umeng.push.notification.NotificationService;
import com.vipyinzhiwei.android.library.umeng.push.notification.UmengNotificationService;

/**
 * 友盟推送，完全自定义消息处理服务类。不使用友盟默认提供的SDK处理。（可选。需注册友盟推送并设置完全自定义处理服务类后使用）
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class MyUmengNotificationService extends UmengNotificationService {
    @Override
    protected Class<? extends NotificationService> getCustomNotificationService() {
//      默认返回提供好的NotificationService类进行相关处理。如需自行处理可以继承NotificationService类重写相关方法。
        return NotificationService.class;
    }

    /**
     * 根据业务需求，继承NotificationService类，重写showNotification方法进行相关处理..
     */
    public static class MyNotificationService extends NotificationService {
        @Override
        protected void showNotification(UMessage msg) {
            super.showNotification(msg);
        }
    }
}
