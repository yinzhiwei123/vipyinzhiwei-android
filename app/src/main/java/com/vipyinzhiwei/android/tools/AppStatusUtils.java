package com.vipyinzhiwei.android.tools;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import com.vipyinzhiwei.android.library.Logger;

import java.util.List;

/**
 * 获取指定包名的 APP 是否还在后台运行，判断 APP 是否存活。
 * <p>
 * 在正式使用的时候需要配合使用，才能覆盖全部情况
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class AppStatusUtils {
    private static final String TAG = "AppStatusUtils";

    /**
     * 判断应用是否已经启动
     *
     * @param context     一个context
     * @param packageName 要判断应用的包名
     * @return boolean
     */
    private boolean isAppAlive(Context context, String packageName) {
        int uid = AppStatusUtils.getPackageUid(context, packageName);
        if (uid > 0) {
            boolean rstA = AppStatusUtils.isAppRunning(context, packageName);
            boolean rstB = AppStatusUtils.isProcessRunning(context, uid);
            if (rstA && rstB) {
                //指定包名的程序正在运行中
                Logger.d(TAG, "正在运行中，" + "rstA=" + rstA + ",rstB=" + rstB);
                return true;
            } else {
                //指定包名的程序未在运行中
                Logger.d(TAG, "未在运行中");
            }
        } else {
            //应用未安装
        }
        return false;
    }

    /**
     * 判断某一应用是否正在运行
     *
     * @param context     上下文
     * @param packageName 应用的包名
     * @return true 表示正在运行，false 表示没有运行
     */
    public static boolean isAppRunning(Context context, String packageName) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(100);
        if (list.size() <= 0) {
            return false;
        }
        for (ActivityManager.RunningTaskInfo info : list) {
            if (info.baseActivity.getPackageName().equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取已安装应用的 uid，-1 表示未安装此应用或程序异常
     *
     * @param context
     * @param packageName
     * @return
     */
    public static int getPackageUid(Context context, String packageName) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo
                    (packageName, 0);
            if (applicationInfo != null) {
                Log.d(TAG, applicationInfo.uid + "");
                return applicationInfo.uid;
            }
        } catch (Exception e) {
            return -1;
        }
        return -1;
    }

    /**
     * 判断某一 uid 的程序是否有正在运行的进程，即是否存活
     *
     * @param context 上下文
     * @param uid     已安装应用的 uid
     * @return true 表示正在运行，false 表示没有运行
     */
    public static boolean isProcessRunning(Context context, int uid) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> runningServiceInfos = am.getRunningServices(200);
        if (runningServiceInfos.size() > 0) {
            for (ActivityManager.RunningServiceInfo appProcess : runningServiceInfos) {
                if (uid == appProcess.uid) {
                    return true;
                }
            }
        }
        return false;
    }
}
