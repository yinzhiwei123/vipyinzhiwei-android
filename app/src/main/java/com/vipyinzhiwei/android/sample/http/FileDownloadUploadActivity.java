/*
 *     Android基础开发个人积累、沉淀、封装、整理共通
 *     Copyright (c) 2016. 曾繁添 <zftlive@163.com>
 *     Github：https://github.com/zengfantian || http://git.oschina.net/zftlive
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.vipyinzhiwei.android.sample.http;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.vipyinzhiwei.android.R;
import com.vipyinzhiwei.android.library.Alert;
import com.vipyinzhiwei.android.library.Logger;
import com.vipyinzhiwei.android.library.base.ui.CommonActivity;
import com.vipyinzhiwei.android.library.network.okhttp.OkHttpUtils;
import com.vipyinzhiwei.android.library.network.okhttp.callback.Callback;
import com.vipyinzhiwei.android.library.network.okhttp.callback.FileCallBack;

import java.io.File;

import okhttp3.Call;
import okhttp3.Response;

/**
 * HTTP带进度上传/下载文件示例,参考：https://github.com/hongyangAndroid/okhttputils
 *
 * @author 曾繁添
 * @version 1.0
 */
public class FileDownloadUploadActivity extends CommonActivity implements OnClickListener {

    private Button btn_download, btn_upload;
    private EditText et_downfile_path, et_upload_file_path;

    public final static String DOWNLOAD_FILE_PATH = "https://ws3.sinaimg" +
            ".cn/large/006tNbRwgy1fuiasmu20wj30sg0sgmxn.jpg";
    public final static String UPLOAD_FILE =
            "http://10.45.255.90:8080/AjavaWeb/cn/com/ajava/servlet/ServletUploadFile";

    @Override
    public int bindLayout() {
        return R.layout.activity_file_upload_download;
    }

    @Override
    public void initParams(Bundle parms) {

    }

    @Override
    public void initView(View view) {
        btn_download = (Button) findViewById(R.id.btn_download);
        btn_download.setOnClickListener(this);

        btn_upload = (Button) findViewById(R.id.btn_upload);
        btn_upload.setOnClickListener(this);

        et_downfile_path = (EditText) findViewById(R.id.et_downfile_path);
        et_downfile_path.setText(DOWNLOAD_FILE_PATH);
        et_upload_file_path = (EditText) findViewById(R.id.et_upload_file_path);
    }

    @Override
    public void doBusiness(Context mContext) {
        //初始化带返回按钮的标题栏
        String strCenterTitle = getResources().getString(R.string.FileDownloadUploadActivity);
//      ActionBarManager.initBackTitle(getContext(), getActionBar(), strCenterTitle);
        mWindowTitle.initBackTitleBar(strCenterTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_download:
                Alert.loading(getContext(), "准备下载");

                OkHttpUtils.get().url(DOWNLOAD_FILE_PATH).build().execute(new FileCallBack
                        (Environment.getExternalStorageDirectory().getAbsolutePath(),
                                "vipyinzhiwei" + ".png") {

                    @Override
                    public void inProgress(float progress, long total, int id) {
                        super.inProgress(progress, total, id);
                        Logger.d(TAG, "progress :" + progress + ",total:" + total + ",id:" + id);
                        if (progress > 0 && total > 0) {
                            Alert.updateProgressText("" + (int) (100 * progress) + "%");
                        }
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        Alert.toastShort(getContext(), "下载成功:" + response.getAbsolutePath());
                        Alert.closeLoading();
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Alert.toastShort(getContext(), "下载失败:" + e.getMessage());
                    }
                });

                break;
            case R.id.btn_upload:
                Alert.loading(getContext(), "准备上传");

                OkHttpUtils.post()
                        .addFile("mFile", "messenger_01.png", null)
                        .addFile("mFile", "test1.txt", null)
                        .url(null)
                        .params(null)
                        .headers(null)
                        .build()
                        .execute(new Callback() {
                            @Override
                            public void inProgress(float progress, long total, int id) {
                                super.inProgress(progress, total, id);
                                if (progress > 0 && total > 0) {
                                    Alert.updateProgressText("" + (int) (100 * progress) + "%");
                                }
                            }

                            @Override
                            public Object parseNetworkResponse(Response response, int id) throws
                                    Exception {
                                return null;
                            }

                            @Override
                            public void onError(Call call, Exception e, int id) {
                                Alert.toastShort(getContext(), "上传失败:" + e.getMessage());
                            }

                            @Override
                            public void onResponse(Object response, int id) {
                                Alert.toastShort(getContext(), "上传成功:" + response);
                            }
                        });

                break;
            default:
                break;
        }
    }
}
