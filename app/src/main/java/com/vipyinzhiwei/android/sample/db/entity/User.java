package com.vipyinzhiwei.android.sample.db.entity;

import com.vipyinzhiwei.android.library.base.adapter.IAdapterModel;
import com.vipyinzhiwei.android.library.db.DBPersistentBean;

/**
 * 首页[菜单]数据
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class User extends DBPersistentBean implements IAdapterModel {

    /**
     *
     */
    private static final long serialVersionUID = -6365235181773183477L;

    /**
     * 序号
     */
    private String orderNo;

    /**
     * 用户名
     */
    private String username;

    /**
     * 电子邮箱
     */
    private String email;


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
