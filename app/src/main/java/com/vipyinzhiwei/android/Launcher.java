package com.vipyinzhiwei.android;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.vipyinzhiwei.android.library.base.ui.CommonActivity;
import com.vipyinzhiwei.android.library.umeng.statistics.UmengMobclickAgent;
import com.vipyinzhiwei.android.sample.MainActivity;

/**
 * 引导界面
 *
 * @author vipyinzhiwei
 * @version 1.0
 */
public class Launcher extends CommonActivity {

    @Override
    public int bindLayout() {
        return R.layout.activity_launcher;
    }

    @Override
    public void initView(View view) {
        //隐藏标题栏
        mWindowTitle.hiddeTitleBar();

        try {
            // 抛出异常的代码
            String strText = null;
            int i = strText.length();
        } catch (Exception e) {
            UmengMobclickAgent.reportError(getContext(), e);
        }

        //添加动画效果
        AlphaAnimation animation = new AlphaAnimation(0.3f, 1.0f);
        animation.setDuration(500);
        animation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //跳转界面
                getOperation().forward(MainActivity.class);
                finish();
                //右往左推出效果
//				overridePendingTransition(R.anim.anl_push_left_in,R.anim.anl_push_left_out);
                //转动淡出效果1
//				overridePendingTransition(R.anim.anl_scale_rotate_in,R.anim.anl_alpha_out);
                //下往上推出效果
                overridePendingTransition(R.anim.anl_push_bottom_in, R.anim.anl_push_up_out);
            }
        });
        view.setAnimation(animation);
    }
}
