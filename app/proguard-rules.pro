# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html
####################注释说明（开始）###############################
#忽略警告
#-ignorewarnings
#抑制错误警告-->找不到com.xx.bbb.**包里面的类的相关引用等等
# -dontwarn
#所有类和所有方法不混淆
# -keep class
#指明lib包的在工程中的路径
# -libraryjars
#是否使用大小写混合
# -dontusemixedcaseclassnames
#指定代码的压缩级别0 ~ 7
# -optimizationpasses 5
#是否使用大小写混合
# -dontusemixedcaseclassnames
#是否混淆第三方jar 如果应用程序引入的有jar包,并且想混淆jar包里面的class
# -dontskipnonpubliclibraryclasses
#混淆时是否做预校验
# -dontpreverify
#混淆时是否记录日志
# -verbose
#混淆时所采用的算法
# -optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
####################注释说明（结束）###############################

#忽略警告
-ignorewarnings
#是否使用大小写混合
-dontusemixedcaseclassnames
#是否混淆第三方jar 如果应用程序引入的有jar包,并且想混淆jar包里面的class（最好不要混淆第三方的jar）
#-dontskipnonpubliclibraryclasses
#指定代码的压缩级别0 ~ 7
-optimizationpasses 5
#混淆时是否记录日志
-verbose

############################################Android默认混淆配置（开始）#############################################
# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).
#是否对类内部代码进行优化，默认优化(如果需要删除日志输出代码则不能配置这个)
# -dontoptimize
-dontpreverify
# Note that if you want to enable optimization, you cannot just
# include optimization flags in your own project configuration file;
# instead you will need to point to the
# "proguard-android-optimize.txt" file instead of this one from your
# project.properties file.

-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**
############################################Android默认混淆配置（结束）#############################################

#####视图模板类库混淆配置####
-keep public class * implements com.vipyinzhiwei.android.library.base.templet.IViewTemplet {
    public <methods>;
}
-keep public class * extends com.vipyinzhiwei.android.library.base.templet.AbsViewTemplet {
    public <methods>;
}
-keep public class * extends com.vipyinzhiwei.android.library.base.adapter.RecyclerViewTemplet {
    public <methods>;
}

############################################Android删除日志输出代码配置（开始）#############################################
# 日志输出
-assumenosideeffects class android.util.Log {
   public static boolean isLoggable(java.lang.String, int);
   public static int v(...);
   public static int i(...);
   public static int w(...);
   public static int d(...);
   public static int e(...);
}
# 系统打印
-assumenosideeffects class java.io.PrintStream{
    public void print(...);
    public void println(...);
}
############################################Android删除日志输出代码配置（结束）#############################################

############################################Android四大组件/基本类-组件/数据模型混淆配置（开始）#############################################
-keep public class * extends android.app.Activity
-keep public class * extends android.app.SherlockActivity
-keep public class * extends android.app.Fragment
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
############################################Android四大组件/基本类-组件/数据模型混淆配置（结束）#############################################


############################################Application/R/V4辅助包混淆配置（开始）#############################################
#Application不进行混淆，拷贝时注意记得修改成自己APP的包
-keep public class com.vipyinzhiwei.android.GlobalApplication

#当前工程的R文件不进行混淆，必须保留
-keep public class com.vipyinzhiwei.android.R
-keep class com.vipyinzhiwei.android.R$* {   *;  }
-keepclassmembers class com.vipyinzhiwei.android.R$* {
    <fields>;
    <methods>;
}

#android辅助包
#-libraryjars libs/android-support-v4.jar
-dontwarn android.support.v4.**
-keep class android.support.v4.**
-keep interface android.support.v4.app.** {*;}
############################################Application/R/V4辅助包混淆配置（结束）#############################################


############################################vipyinzhiwei-xxx或者vipyinzhiwei-Android-lib-Vxxx.jar（开始）#############################################
#类库R文件不能混淆，必须保留
-keep public class com.vipyinzhiwei.android.library.R
-keep class com.vipyinzhiwei.android.library.R$* {   *;  }
-keepclassmembers class com.vipyinzhiwei.android.library.R$* {
    <fields>;
    <methods>;
}
#网络检测类（防止找不到构造方法）
-keep public class com.vipyinzhiwei.android.library.tools.ToolNetwork
-keepclassmembers class com.vipyinzhiwei.android.library.tools.ToolNetwork{
    public *;
}
#分享工具类（防止找不到构造方法）
-keep public class com.vipyinzhiwei.android.library.tools.ToolShareSDK
-keepclassmembers class com.vipyinzhiwei.android.library.tools.ToolShareSDK{
    public *;
}
#数据库操作类（防止找不到构造方法）
-keep public class com.vipyinzhiwei.android.library.tools.ToolDatabase
-keepclassmembers class com.vipyinzhiwei.android.library.tools.ToolDatabase{
    public *;
}
#保留所有序列化的实体类名、字段、方法，使用了GSON或者数据库映射
-keep public class * extends com.vipyinzhiwei.android.library.base.BaseEntity
-keepclasseswithmembers class * extends com.vipyinzhiwei.android.library.base.BaseEntity{
    <fields>;
    <methods>;
}
#自定义控件保留，防止找不到类（使用包名通配和类继承关系）
-keep public class * extends com.vipyinzhiwei.android.library.base.BaseView
-keepclasseswithmembers class * extends com.vipyinzhiwei.android.library.base.BaseView{
    <fields>;
    <methods>;
}
-dontwarn com.vipyinzhiwei.android.library.widget.**
-keep class com.vipyinzhiwei.android.library.widget.** {*;}
#集成的第三方开源类库不混淆
-dontwarn com.vipyinzhiwei.android.library.third.**
-keep class com.vipyinzhiwei.android.library.third.** {*;}

############################################vipyinzhiwei-xxx或者vipyinzhiwei-Android-lib-Vxxx.jar（结束）#############################################


############################################第三方jar混淆保留配置（开始）#############################################
####################友盟统计分析（开始）#####################
-keep class com.umeng.** {*;}

-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
####################友盟统计分析（结束）#####################

####################友盟分享（开始）#####################
-dontshrink
-dontoptimize
-dontwarn com.google.android.maps.**
-dontwarn android.webkit.WebView
-dontwarn com.umeng.**
-dontwarn com.tencent.weibo.sdk.**
-dontwarn com.facebook.**
-keep public class javax.**
-keep public class android.webkit.**
-dontwarn android.support.v4.**
-keep enum com.facebook.**
-keepattributes Exceptions,InnerClasses,Signature
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable

-keep public interface com.facebook.**
-keep public interface com.tencent.**
-keep public interface com.umeng.socialize.**
-keep public interface com.umeng.socialize.sensor.**
-keep public interface com.umeng.scrshot.**

-keep public class com.umeng.socialize.* {*;}


-keep class com.facebook.**
-keep class com.facebook.** { *; }
-keep class com.umeng.scrshot.**
-keep public class com.tencent.** {*;}
-keep class com.umeng.socialize.sensor.**
-keep class com.umeng.socialize.handler.**
-keep class com.umeng.socialize.handler.*
-keep class com.umeng.weixin.handler.**
-keep class com.umeng.weixin.handler.*
-keep class com.umeng.qq.handler.**
-keep class com.umeng.qq.handler.*
-keep class UMMoreHandler{*;}
-keep class com.tencent.mm.sdk.modelmsg.WXMediaMessage {*;}
-keep class com.tencent.mm.sdk.modelmsg.** implements com.tencent.mm.sdk.modelmsg.WXMediaMessage$IMediaObject {*;}
-keep class im.yixin.sdk.api.YXMessage {*;}
-keep class im.yixin.sdk.api.** implements im.yixin.sdk.api.YXMessage$YXMessageData{*;}
-keep class com.tencent.mm.sdk.** {
   *;
}
-keep class com.tencent.mm.opensdk.** {
   *;
}
-keep class com.tencent.wxop.** {
   *;
}
-keep class com.tencent.mm.sdk.** {
   *;
}
-dontwarn twitter4j.**
-keep class twitter4j.** { *; }

-keep class com.tencent.** {*;}
-dontwarn com.tencent.**
-keep class com.kakao.** {*;}
-dontwarn com.kakao.**
-keep public class com.umeng.com.umeng.soexample.R$*{
    public static final int *;
}
-keep public class com.linkedin.android.mobilesdk.R$*{
    public static final int *;
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class com.tencent.open.TDialog$*
-keep class com.tencent.open.TDialog$* {*;}
-keep class com.tencent.open.PKDialog
-keep class com.tencent.open.PKDialog {*;}
-keep class com.tencent.open.PKDialog$*
-keep class com.tencent.open.PKDialog$* {*;}
-keep class com.umeng.socialize.impl.ImageImpl {*;}
-keep class com.sina.** {*;}
-dontwarn com.sina.**
-keep class  com.alipay.share.sdk.** {
   *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keep class com.linkedin.** { *; }
-keep class com.android.dingtalk.share.ddsharemodule.** { *; }
-keepattributes Signature
####################友盟分享（结束）#####################

####################友盟推送（开始）#####################
-dontwarn com.umeng.**
-dontwarn com.taobao.**
-dontwarn anet.channel.**
-dontwarn anetwork.channel.**
-dontwarn org.android.**
-dontwarn org.apache.thrift.**
-dontwarn com.xiaomi.**
-dontwarn com.huawei.**
-dontwarn com.meizu.**

-keepattributes *Annotation*

-keep class com.taobao.** {*;}
-keep class org.android.** {*;}
-keep class anet.channel.** {*;}
-keep class com.umeng.** {*;}
-keep class com.xiaomi.** {*;}
-keep class com.huawei.** {*;}
-keep class com.meizu.** {*;}
-keep class org.apache.thrift.** {*;}

-keep class com.alibaba.sdk.android.**{*;}
-keep class com.ut.**{*;}
-keep class com.ta.**{*;}

-keep public class **.R$*{
   public static final int *;
}
####################友盟推送（结束）#####################

####################谷歌开源项目相关zxing/gson/volley/RoboGuice2（开始）#####################

#谷歌GSON
-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

####################谷歌开源项目相关zxing/gson/volley/RoboGuice2（结束）#####################

####################SOAP访问第三方jar ksoap2-android.jar（开始）#####################
-dontwarn org.kobjects.**
-keep class org.kobjects.** {*;}
-dontwarn org.ksoap2.**
-keep class org.ksoap2.** {*;}
-dontwarn org.kxml2.**
-keep class org.kxml2.** {*;}
-dontwarn org.xmlpull.**
-keep class org.xmlpull.** {*;}
####################SOAP访问第三方jar ksoap2-android.jar（结束）#####################

####################nineoldandroids（开始）#####################
-dontwarn com.nineoldandroids.**
-keep class com.nineoldandroids.** {*;}
####################nineoldandroids（结束）#####################

####################ActionBarSherlock相关jar（开始）####################
#-dontwarn com.actionbarsherlock.**
#-keep class com.actionbarsherlock.** {*;}
####################ActionBarSherlock相关jar（结束）####################

####################图片加载glide4.0（开始）#####################
#glide4.0
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# 从glide4.0开始，GifDrawable没有提供getDecoder()方法，
# 需要通过反射获取gifDecoder字段值，所以需要保持GifFrameLoader和GifState类不被混淆
-keep class com.bumptech.glide.load.resource.gif.GifDrawable$GifState{*;}
-keep class com.bumptech.glide.load.resource.gif.GifFrameLoader {*;}
####################图片加载glide4.0（结束）#####################

####################LitePal数据库操作相关jar（开始）#####################
-keep class org.litepal.** {
    *;
}

-keep class * extends org.litepal.crud.DataSupport {
    *;
}

-keep class * extends org.litepal.crud.LitePalSupport {
    *;
}
####################LitePal数据库操作相关jar（结束）#####################

####################网络通信相关jar（开始）###########################

#okhttp
-dontwarn okhttp3.**
-keep class okhttp3.**{*;}
-keep interface okhttp3.**{*;}

#okio
-dontwarn okio.**
-keep class okio.**{*;}
-keep interface okio.**{*;}

#okhttputils
-dontwarn com.vipyinzhiwei.android.library.network.**
-keep class com.vipyinzhiwei.android.library.network.**{*;}
-keep interface com.vipyinzhiwei.android.library.network.**{*;}

# Retrofit
-keep class retrofit2.** { *; }
-dontwarn retrofit2.**
-keepattributes Signature
-keepattributes Exceptions
-dontwarn okio.**
-dontwarn javax.annotation.**

####################网络通信相关jar（结束）############################

####################asmack操作相关jar（开始）#####################
#-dontwarn com.kenai.jbosh.**
#-keep class com.kenai.jbosh.** {*;}
#-dontwarn com.novell.sasl.client.**
#-keep class com.novell.sasl.client.** {*;}
#-dontwarn de.measite.smack.**
#-keep class de.measite.smack.** {*;}
#-dontwarn org.xbill.DNS.**
#-keep class org.xbill.DNS.** {*;}
#-dontwarn org.jivesoftware.smack.**
#-keep class org.jivesoftware.smack.** {*;}
#-dontwarn org.jivesoftware.smackx.**
#-keep class org.jivesoftware.smackx.** {*;}
#-dontwarn org.apache.qpid.management.common.sasl.**
#-keep class org.apache.qpid.management.common.sasl.** {*;}
#-dontwarn org.apache.harmony.javax.security.**
#-keep class org.apache.harmony.javax.security.** {*;}
####################asmack操作相关jar（结束）#####################

####################poi辅助包（开始）#####################
#-libraryjars libs/poi-3.9-20121203.jar
#-libraryjars libs/poi-scratchpad-3.9-20121203.jar
#-dontwarn org.apache.poi.**
#-keep class org.apache.poi.** {*;}
####################poi辅助包（结束）#####################

####################表单验证插件 android-validation-komensky0.9.3.jar（开始）#####################
#-dontwarn eu.inmite.android.lib.**
#-keep class eu.inmite.android.lib.** {*;}
####################表单验证插件 android-validation-komensky0.9.3.jar（结束）#####################

####################XStream操作xml与Bean之间转换jar（开始）#####################
#-dontwarn com.thoughtworks.xstream.**
#-keep class com.thoughtworks.xstream.** {*;}
####################XStream操作xml与Bean之间转换jar（结束）#####################

############################################第三方jar混淆保留配置（结束）#############################################